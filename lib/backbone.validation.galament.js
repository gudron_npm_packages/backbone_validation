(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("backbone-validation");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(2);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _version = __webpack_require__(3);

var _version2 = _interopRequireDefault(_version);

var _backboneValidation = __webpack_require__(0);

var _backboneValidation2 = _interopRequireDefault(_backboneValidation);

var _underscore = __webpack_require__(4);

var _underscore2 = _interopRequireDefault(_underscore);

var _common = __webpack_require__(5);

var _common2 = _interopRequireDefault(_common);

var _inModel = __webpack_require__(6);

var _inModel2 = _interopRequireDefault(_inModel);

var _date = __webpack_require__(7);

var _date2 = _interopRequireDefault(_date);

var _mobilePhone = __webpack_require__(8);

var _mobilePhone2 = _interopRequireDefault(_mobilePhone);

var _cyrillic = __webpack_require__(9);

var _cyrillic2 = _interopRequireDefault(_cyrillic);

var _isoCalendarDate = __webpack_require__(10);

var _isoCalendarDate2 = _interopRequireDefault(_isoCalendarDate);

var _cyrillic3 = __webpack_require__(11);

var _cyrillic4 = _interopRequireDefault(_cyrillic3);

var _mobilePhone3 = __webpack_require__(12);

var _mobilePhone4 = _interopRequireDefault(_mobilePhone3);

var _date3 = __webpack_require__(13);

var _date4 = _interopRequireDefault(_date3);

var _inModel3 = __webpack_require__(15);

var _inModel4 = _interopRequireDefault(_inModel3);

var _label = __webpack_require__(16);

var _label2 = _interopRequireDefault(_label);

var _format = __webpack_require__(17);

var _format2 = _interopRequireDefault(_format);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Formatters


// Validators


// Validator messages
var patterns = _underscore2.default.extend({}, _backboneValidation2.default.patterns, _cyrillic4.default, _mobilePhone4.default, _isoCalendarDate2.default);

// Validator patterns
// Galament Backbone Validation
// ---------

var messages = _underscore2.default.extend({}, _backboneValidation2.default.messages, _common2.default, _inModel2.default, _date2.default, _mobilePhone2.default, _cyrillic2.default);

var validators = _underscore2.default.extend({}, _backboneValidation2.default.validators, _date4.default, _inModel4.default, _format2.default);

var labelFormatters = _underscore2.default.extend({}, _label2.default);

_underscore2.default.extend(Backbone.Validation.patterns, patterns);
_underscore2.default.extend(Backbone.Validation.messages, messages);
_underscore2.default.extend(Backbone.Validation.validators, validators);
_underscore2.default.extend(Backbone.Validation.labelFormatters, labelFormatters);

Backbone.Validation.configure({
    labelFormatter: 'label'
});

exports.default = _backboneValidation2.default;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = {"version":"0.0.6","parentLibraryName":"backbone-validation","parentLibraryVersion":"0.11.5","buildDate":"2017-11-27"}

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("underscore");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    required: 'error.required',
    acceptance: 'error.acceptance',
    min: 'error.min',
    max: 'error.max',
    range: 'error.range',
    length: 'error.length',
    minLength: 'error.minLength',
    maxLength: 'error.maxLength',
    rangeLength: 'error.rangeLength',
    oneOf: 'error.oneOf',
    equalTo: 'error.equalTo',
    digits: 'error.digits',
    number: 'error.number',
    email: 'error.email',
    url: 'error.url',
    inlinePattern: 'error.inlinePattern'
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    greaterThenField: 'error.greaterThenField',
    lowerThenField: 'error.lowerThenField',
    manyOf: 'error.manyOf'
};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    minAge: 'error.minAge',
    maxAge: 'error.maxAge',
    rangeAge: 'error.rangeAge'
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    mobile: 'error.mobile'
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    cyrillic: 'error.cyrillic'
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    'date:YYYY-MM-DD': /\d{4}-\d\d-\d\d/,
    'date:YYYY.MM.DD': /\d{4}\.\d\d\.\d\d/,
    'date:YYYYYY-MM-DD': /[+-]\d{6}-\d\d-\d\d/,
    'date:YYYYYY.MM.DD': /[+-]\d{6}\.\d\d\.\d\d/,
    'date:GGGG-[W]WW-E': /\d{4}-W\d\d-\d/,
    'date:GGGG-[W]WW': /\d{4}-W\d\d/,
    'date:YYYY-DDD': /\d{4}-\d{3}/,
    'date:YYYY-MM': /\d{4}-\d\d/,
    'date:YYYYYYMMDD': /[+-]\d{10}/,
    'date:YYYYMMDD': /\d{8}/
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    cyrillic: /^[а-яА-ЯЁё]+$/
};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    mobile: /^[\+0-9\s\-\(\)]+$/ig
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _moment = __webpack_require__(14);

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    minAge: function minAge(value, attr, customValue, model) {
        var defaultMessages = Backbone.Validation.messages;

        var acceptanceDate = (0, _moment2.default)().subtract(customValue, 'years');

        var inputDate = (0, _moment2.default)(value);

        if (acceptanceDate.isBefore(inputDate)) {
            return this.format(defaultMessages.minAge, this.formatLabel(attr, model), customValue);
        }
    },
    maxAge: function maxAge(value, attr, customValue, model) {
        var defaultMessages = Backbone.Validation.messages;

        var acceptanceDate = (0, _moment2.default)().subtract(customValue, 'years');

        var inputDate = (0, _moment2.default)(value);

        if (acceptanceDate.isAfter(inputDate)) {
            return this.format(defaultMessages.maxAge, this.formatLabel(attr, model), customValue);
        }
    },
    rangeAge: function rangeAge(value, attr, customValue, model) {
        var defaultMessages = Backbone.Validation.messages;

        var minAcceptanceDate = (0, _moment2.default)().subtract(customValue[0], 'years');

        var maxAcceptanceDate = (0, _moment2.default)().subtract(customValue[1], 'years');

        var inputDate = (0, _moment2.default)(value);

        if (!inputDate.isBetween(maxAcceptanceDate, minAcceptanceDate)) {
            return this.format(defaultMessages.rangeAge, this.formatLabel(attr, model), customValue[0], customValue[1]);
        }
    }
};

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    greaterThenField: function greaterThenField(value, attr, greaterThenAttr, model) {
        var defaultMessages = Backbone.Validation.messages;

        if (value < parseInt(model.get(greaterThenAttr))) {
            return this.format(defaultMessages.greaterThenField, this.formatLabel(attr, model), this.formatLabel(greaterThenAttr, model));
        }
    },
    lowerThenField: function lowerThenField(value, attr, lowerThenAttr, model) {
        var defaultMessages = Backbone.Validation.messages;

        if (value > parseInt(model.get(lowerThenAttr))) {
            return this.format(defaultMessages.lowerThenField, this.formatLabel(attr, model), this.formatLabel(lowerThenAttr, model));
        }
    },
    manyOf: function manyOf(values, attr, customValues, model) {
        var defaultMessages = Backbone.Validation.messages;
        var allowedList = _.isFunction(customValues) ? customValues.call(null) : customValues;

        var result = _.every(values, function (value) {
            return _.indexOf(allowedList, value) != -1;
        });

        if (!result) {
            return this.format(defaultMessages.manyOf, this.formatLabel(attr, model));
        }
    }
};

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backboneValidation = __webpack_require__(0);

var _backboneValidation2 = _interopRequireDefault(_backboneValidation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    label: function label(attrName, model) {
        return model.labels && _.result(model, 'labels')[attrName] || Backbone.Validation.labelFormatters.sentenceCase(attrName, model);
    }
};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    // Replaces nummeric placeholders like {0} in a string with arguments
    // passed to the function
    format: function format() {
        var args = Array.prototype.slice.call(arguments),
            text = App.t(args.shift());
        return text.replace(/\{(\d+)\}/g, function (match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    }
};

/***/ })
/******/ ]);
});
//# sourceMappingURL=backbone.validation.galament.js.map