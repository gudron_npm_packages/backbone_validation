# Changelog

## [Unreleased]

## [v0.0.6] - 2017-11-27
### Added
- Label formatter configure

## [v0.0.5] - 2017-11-27
### Removed
- Overwrite format function

## [v0.0.4] - 2017-11-22
### Added
- Overwrite format function
### Fixed
- Call translation function for error messaging

## [v0.0.3] - 2017-11-21
### Changed
- Basic Validation plugin extend

### Fixed
- Importing Marionette framework

## [v0.0.2] - 2017-11-20
### Fixed
- Importing Marionette framework

## [v0.0.1] - 2017-11-20
### Added
- Project init
- Changelog file
- Semantic versioning